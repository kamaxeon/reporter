# Reporter

Reporter is a tool to generate and send reports from data in KCIDB format.

## Maintainers

[Ondřej Kinšt](https://gitlab.com/Toaster192)

At least one free spot here!

## Running the project

Run with `python -m reporter`.

See `python -m reporter --help` for an updated list of all possible parameters.

The project also reads from environment variables such as `RABBITMQ_HOST`,
`RABBITMQ_PORT` or `DATAWAREHOUSE_URL`. To see all variables that can be set
this way, see [the settings.py file](reporter/settings.py)


## Developer guidelines

The reporter should have as little logic as possible. This means it should only
really be parsing data received from the
[datawarehouse](https://gitlab.com/cki-project/datawarehouse).

When adding more logic, consider adding it to the datawarehouse (or other
component of the pipeline) instead.

## Development

Before submitting a merge request with changes, run tests and linting locally.
To do this use the `tox` command.

## Tests before merging changes

When submitting a merge request that reduces the tests code coverage, please
consider adding tests to counteract this.

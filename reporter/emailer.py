"""Send emails with reports."""
from email.message import EmailMessage
import email.utils
import smtplib

from cki_lib import misc

from . import settings
from .utils import join_addresses
from .utils import status_to_emoji


def render_template(checkout_data, template_name=None):
    """Render the report template using checkout data."""
    if template_name is None:
        template_name = settings.SELECTED_TEMPLATE

    template_filename = settings.TEMPLATES[template_name]
    template = settings.JINJA_ENV.get_template(template_filename)
    context = {'checkout_data': checkout_data,
               'build_data': checkout_data.build_data,
               'test_data': checkout_data.test_data,
               'is_email': settings.EMAILING_ENABLED,
               'dw_url': settings.DATAWAREHOUSE_URL}
    return template.render(context).strip()


def get_subject(checkout_data):
    """Create the subject of the email."""
    result = 'PASS' if checkout_data.result else 'FAIL'

    test_summary = ""
    if checkout_data.result and checkout_data.test_data.tests:
        skipped_test_count = len(checkout_data.test_data.skipped_tests)
        total_test_count = len(checkout_data.test_data.tests)
        test_summary = f" ({skipped_test_count}/{total_test_count} SKIPPED)"

    version = checkout_data.checkout.misc.get('kernel_version')
    if checkout_data.checkout.patchset_hash:
        commit_hash = checkout_data.checkout.patchset_hash[:8]
    elif checkout_data.checkout.git_commit_hash:
        commit_hash = checkout_data.checkout.git_commit_hash[:8]
    else:
        commit_hash = None

    subject = (f'{status_to_emoji(result)} {result}{test_summary}: '
               'Test report for ')

    # Gather all the items that can be used to identify a checkout
    info_items = [checkout_data.checkout.git_repository_branch, version,
                  checkout_data.checkout.tree_name, commit_hash]
    info_items = list(filter(lambda x: x is not None, info_items))
    # Use the one that is hopefully the most descriptive
    subject += info_items.pop(0)
    # and put the rest in parenthesis
    if info_items:
        subject += ' (' + ', '.join(info_items) + ')'

    return subject


def add_archive_address(recipients):
    """Add the archive address to BCC recipients if present."""
    if settings.BCC_ARCHIVE_ADDRESS:
        recipients.setdefault('BCC', []).append(settings.BCC_ARCHIVE_ADDRESS)


def build_email_headers(checkout_data, recipients):
    """Build the email message object."""
    msg = EmailMessage()

    # Set up the required email fields.
    msg['From'] = settings.REPORTER_EMAIL_FROM
    msg['Subject'] = get_subject(checkout_data)
    msg['Date'] = email.utils.formatdate()

    add_archive_address(recipients)
    for target, emails in recipients.items():
        msg[target] = join_addresses(emails)

    return msg


def merge_recipients(target, source):
    """Merge the source dictionary with the target dictionary."""
    for key, value in source.items():
        if key not in target:
            target[key] = value
        else:
            target[key] += value


def set_default_template(recipients_by_template):
    """If the recipients contain 'default' for the template, set it to the default template."""
    if 'default' in recipients_by_template:
        default = recipients_by_template.pop('default')
        if settings.SELECTED_TEMPLATE in recipients_by_template:
            merge_recipients(recipients_by_template[settings.SELECTED_TEMPLATE], default)
        else:
            recipients_by_template[settings.SELECTED_TEMPLATE] = default


def prepare_recipients(checkout_data):
    """Request the recipients and convert them into a dictionary so they are easier to work with."""
    recipients = checkout_data.checkout.recipients.get()
    # Convert to a dictionary to make it easier to work with
    recipients = recipients.attributes

    set_default_template(recipients)
    # Remove any keys that are not template names
    recipients = {key: value for key, value in recipients.items() if key in settings.TEMPLATES}
    return recipients


def send_emails(checkout_data):
    """Send an email for each template."""
    # Build the email content and headers.
    recipients_by_template = prepare_recipients(checkout_data)

    # Set up the SMTP connection and debug level.
    smtp_conn = smtplib.SMTP(settings.SMTP_URL)
    if settings.SMTP_DEBUG:
        smtp_conn.set_debuglevel(2)

    for template, recipients in recipients_by_template.items():
        if template in settings.FAILURE_ONLY_TEMPLATES and checkout_data.result:
            # Skip in case of failure only template and the checkout did not fail
            continue

        msg = build_email_headers(checkout_data, recipients)
        content = render_template(checkout_data, template)
        msg.set_content(content)

        if misc.is_production():
            smtp_conn.send_message(msg)
        else:
            settings.LOGGER.info(msg)

    smtp_conn.quit()
